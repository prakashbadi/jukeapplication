package com.example.JuckBox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JuckBoxApplication {

	public static void main(String[] args) {
		SpringApplication.run(JuckBoxApplication.class, args);
	}

}
